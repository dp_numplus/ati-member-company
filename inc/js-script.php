﻿	<script src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
	<script type="text/javascript" src="js/jquery.sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript" src="js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
	<script type="text/javascript" src="js/behaviour/general.js"></script>
	<script src="js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.nestable/jquery.nestable.js"></script>
	<script type="text/javascript" src="js/bootstrap.switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="js//bootstrap.datepicker/bootstrap-datepicker.js"></script>
    <script src="js//bootstrap.datepicker/bootstrap-datepicker-thai.js"></script>
    <script src="js//bootstrap.datepicker/locales/bootstrap-datepicker.th.js"></script>
	<script src="js/jquery.select2/select2.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.gritter/js/jquery.gritter.js"></script>
  	<script type="text/javascript" src="js/jquery.niftymodals/js/jquery.modalEffects.js"></script>   
	<!--insert datatable core-->
	<script type="text/javascript" charset="utf-8" src="media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8" src="media/js/ZeroClipboard.js"></script>
	<script type="text/javascript" charset="utf-8" src="media/js/TableTools.js"></script>
	<!--insert fancybox css core-->
	<link rel="stylesheet" href="js/fancyapps/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="js/fancyapps/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript" src="js/jquery.icheck/icheck.min.js"></script>
	<!-- application script for jquery validate demo -->
	<script src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/jquery.maskedinput/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="js/lib.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
		    //initialize the javascript
		    App.init();
    		$("li.dropdown").find('ul > li.active').parent().parent().addClass('active');
		});
	</script>

<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/behaviour/voice-commands.js"></script>
	<script src="js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript" src="js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript" src="js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript" src="js/jquery.flot/jquery.flot.labels.js"></script>	
