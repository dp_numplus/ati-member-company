<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

?>
<div id="cl-wrapper" style="<?php echo ($_GET['p']=="default") ? 'padding-top:30px;' : '' ?>">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcontx">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">						
						<h3><i class="fa fa-list"></i> &nbsp;ประกาศ</h3>
						<div class="pull-right">
							<a href="index.php?p=login">เข้าสู่ระบบ</a>&nbsp;&nbsp;
							<a href="index.php?p=register">สมัครงาน</a>
						</div>
					</div>
					<table id="tbNews" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="8%">ลำดับ</th>
								  <th width="12%">รหัส</th>
								  <th width="">หัวข้อประกาศ</th>
								  <th width="11%">รายละเอียด</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	listItem();	
});

function listItem(){
   var url = "data/news-anchorlist.php";
   oTable = $("#tbNews").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"view_news","value":"T"});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&news_anchor_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

</script>