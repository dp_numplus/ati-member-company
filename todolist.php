<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db, $RIGHTTYPEID, $EMPID;
$todotype = datatype(" and a.active='T'", "todotype", true);
$company = datatype(" and a.active='T'", "company", true);
$todostatus = datatype(" and a.active='T'", "todostatus", true);
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> Add News Todo</button>
						<h3><i class="fa fa-list"></i> &nbsp;Todo History</h3>
					</div>
						<div class="header">

							<div class="form-group row">
								<label class="col-sm-1 control-label">Company<span class="red">*</span></label>
								<div class="col-sm-3">
									<select name="company_id" id="company_id" class="select2" onchange="reCall();">
										<option value="">---- Select ----</option>
										<?php foreach ($company as $key => $value) {
											$id = $value['company_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label" >Assign To<span class="red">*</span></label>
								<div class="col-sm-3" id="load_box">
									<select name="assign_to_id" id="assign_to_id" class="select2" onchange="reCall();">
										<option value="">---- Select ----</option>
										<?php 
										$q = "select * from emp where active='T'";
										$emp = $db->get($q);
										foreach ($emp as $key => $value) {
											$id = $value['emp_id'];
											$s = "";
											if($EMPID==$id){
												$s = "selected";
											}
											$name = $value['prefix'].$value["fname"]." ".$value["lname"];
											echo  "<option value='$id' {$s}>$name</option>";
										} ?>

									</select>
								</div>              
								<label class="col-sm-1 control-label" style="text-align:right;">Status<span class="red">*</span></label>
								<div class="col-sm-3">
									<select name="todostatus_id" id="todostatus_id" class="select2" onchange="reCall();">
								  			<option value="">---- Select ----</option>
								  			<?php foreach ($todostatus as $key => $value) {
								  				$id = $value['todostatus_id'];
								  				$name = $value['name'];
								  				echo  "<option value='$id'>$name</option>";
								  			} ?>

									</select>
								</div> 
							</div>            
								<div class="form-group row">
									<label class="col-sm-1 control-label">Target Date</label>
									<div class="col-sm-3">
										<input class="form-control" name="target_date" id="target_date" placeholder="Target Date" type="text">
									</div> 									
									<label class="col-sm-1 control-label" style="padding-right:2px;">Create Date</label>
									<div class="col-sm-3">
										<input class="form-control" name="created_date" id="created_date" placeholder="Create Date" type="text">
									</div> 																		
								<label class="col-sm-1 control-label"> <a href="#" class="btn btn-rad btn-info" onClick="reCall();"><i class="fa fa-search"></i></a></label>  
								</div>
						</div>
						<br>
					<table id="tbtodo" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="5%">No</th>
								  <th width="8%">Symbol</th>
								  <th >Title</th>
								  <th width="12%">Assign To</th>
								  <th width="10%">Target Date</th>
								  <th width="10%">Create Date</th>
								  <th width="8%">Status</th>
								  <th width="5%">Action</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
    $("#target_date").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#created_date").datepicker({language:'th-th',format:'dd-mm-yyyy'});  
	var oTable;
	$("#frmMain").validate();
	listItem();	
});

function listItem(){
   var url = "data/todolist.php";
   oTable = $("#tbtodo").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"target_date","value":$("#target_date").val()});
			aoData.push({"name":"created_date","value":$("#created_date").val()});
			aoData.push({"name":"todostatus_id","value":$("#todostatus_id").val()});
			aoData.push({"name":"assign_to_id","value":$("#assign_to_id").val()});
			aoData.push({"name":"company_id","value":$("#company_id").val()});	
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&todo_id="+id+"&type=info";
   redirect(url);
}

function viewInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&todo_id="+id+"&type=view";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

</script>