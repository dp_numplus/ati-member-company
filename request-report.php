<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/request.php";
include_once "./share/item.php";
include_once "./share/datatype.php";
global $db;

$request_id = $_GET["request_id"];
$info = get_request("", $request_id);
if($info) $info = $info[0];
$roomname = $db->data("select name from room where room_id={$info["room_id"]}");
?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<style>
	body  {
		 font-family:'AngsanaUPC','Angsana UPC',sans-serif;
		 font-size:14pt;
		 font-style:normal;
		 font-weight:normal;		
		 
	}
	 span , label, p {
		 font-family:'AngsanaUPC','Angsana UPC',sans-serif;
		 font-size:14pt;
		 font-style:normal;
		 font-weight:normal;		
	}
	.control-label span, .header{
		font-weight: bold;
	}
	h1,h2,h3,h4,h5,h6{
		 font-family:'AngsanaUPC','Angsana UPC',sans-serif;
		 font-style:normal;
		 font-weight:normal;		
	}
	table tr th,
	table tr td{
		font-family:'AngsanaUPC','Angsana UPC',sans-serif;
		font-size: 16px;
	}
	#page[size="A4"] {
	position: relative;
	  background: white;
	  width: 21cm;
	  height: 29.7cm;
	  display: block;
	  margin: 0 auto;
	  margin-bottom: 0.5cm;
	  
	}
	@media print {
	  body, #page[size="A4"] {
	    margin: 0;
	    padding: 0px;
	    box-shadow: 0;
	  }
	  #btPrint{
	  	display: none;
	  }
}
</style>
<body>
<div id="page" size="A4">
 <div style=" position:absolute;z-index:1;" id="btPrint">
	 <a href="#" onclick="printPage();" ><span class="fa fa-print" title="print Report">&nbsp;</span></a>
	 <a href="#" onclick="exportToword();" ><span class="fa fa-file" title="export to word">&nbsp;</span></a>
 </div>
	<div class="block-flat">
		<div class="content-pages">
			<div class="form-group row">
				<div class="header" style="text-align:center; margin-bottom:10px;">
					<?php 
					$path = 'images/logo-small.png';
/*					$type = pathinfo($path, PATHINFO_EXTENSION);
					$data = file_get_contents($path);
					$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);*/
					?>
					<img src="http://localhost/sos/<?php echo $path; ?> " alt=""><br><div class="clear"></div>
					แบบฟอร์มการขอใช้บริการหน่วยโสตทัศนูปกรณ์ <br>
					งานพัฒนาระบบและเทคโนโลยี คณะวิทยาศาสตร์ มหาวิทยาลัยมหิดล โทร 02-201-5464 </div>	
				</div>
				<div class="form-group row">
					<label class="col-sm-3 control-label"><span>เลขที่</span> &nbsp;:&nbsp;  <?php echo $info["docno"]; ?></label>
					<label class="col-sm-3 control-label"><span>วันที่</span> &nbsp;:&nbsp; <?php echo revert_date($info["docdate"]); ?></label>
					<label class="col-sm-6 control-label"><span>ชื่อ-นามสกุล</span> &nbsp;:&nbsp;  <?php echo $info["membername"]; ?></label>        
				</div> 							
				<div class="form-group row">
					<label class="col-sm-3 control-label"><span>ประเภทสมาชิก</span> &nbsp;:&nbsp; <?php echo $info["membertype_name"]; ?></label>
					<label class="col-sm-6 control-label"><span>ภาควิชา / หน่วยงาน</span> &nbsp;:&nbsp; <?php echo $info["department_name"]; ?></label>
					<label class="col-sm-3 control-label"><span>เบอร์โทร</span>  &nbsp;:&nbsp; <?php echo $info["dept_phone"]; ?></label>							          
				</div> 							
				<div class="form-group row">
					<label class="col-sm-4 control-label" ><span>จุดประสงค์เพื่อจัด (ชื่อกิจกรรม / โครงการ)</span> </label>
					<label class="col-sm-8 control-label" ><?php echo $info["title"]; ?></label>
				</div>
				<div class="form-group row">
					<div class="col-sm-12">									
						<h3 style="font-weight:bold;font-size:20px;">&nbsp;มีความประสงค์ขอใช้บริการ</h3> 
					</div>
					<label class="col-sm-3 control-label" style="text-align:left;"><?php echo ($info["request_item"]=="T") ? "&#9745;" : "&#9744;"; ?>&nbsp;ยืมอุปกรณ์โสตฯ </label>
					<label class="col-sm-3 control-label" ><?php echo ($info["request_camera"]=="T") ? "&#9745;" : "&#9744;"; ?>&nbsp;ถ่ายภาพนิ่งดิจิตอล </label>
					<label class="col-sm-2 control-label" ><?php echo ($info["request_video"]=="T") ? "&#9745;" : "&#9744;"; ?>&nbsp;ถ่ายทำวีดิทัศน์</label>
					<label class="col-sm-2 control-label" ><?php echo ($info["request_audio"]=="T") ? "&#9745;" : "&#9744;"; ?>&nbsp;บันทึกเสียง</label>
					<label class="col-sm-2 control-label" ><?php echo ($info["request_editing_video"]=="T") ? "&#9745;" : "&#9744;"; ?>&nbsp;ตัดต่อวีดีโอ </label>									
					<label class="col-sm-12 control-label" ><span>สถานที่</span>  &nbsp;:&nbsp; <?php echo $roomname; ?> </label>									
				</div>
				<?php 
					$con = " and a.request_id={$request_id}";
					$r = get_request_detail($con);
					if($r){
				 ?>
				<div class="form-group row">
					<div class="col-sm-12">

						<table >
							<thead>
								<tr style="font-weight:bold;">
									<th width="10%">ลำดับ</th>
									<th width="20%" class="center">รหัส</th>
									<th width="30%" class="center">รายการ</th>
									<th width="10%" class="center">จำนวน</th>
									<th width="30%" class="center">ยี่ห้อ/รุ่น</th>
								</tr>
							</thead>
							<tbody>
								<?php 

								$i = 1;

								foreach($r as $k=>$v){?>
								<tr>
									<td><?php echo $i; ?></td>												
									<td  class="center"><?php echo $v["code"]; ?></td>
									<td  class="center" id="name"><?php echo $v["name"]; ?></td>
									<td  class="center"><?php echo $v["amount"]; ?></td>
									<td  class="center" id="itemtype_name"><?php echo $v["itemtype_name"]; ?></td>													
								</tr>
								<?php 
								$i++;
							} 
							?>
						</tbody>
					</table>								
				</div>
			</div>
			<?php }
			 ?>
			<div class="form-group row">
				<label class="col-sm-4 control-label" ><span>รายละเอียดเพิ่มเติมอื่นๆ ถ้ามี(โปรดระบุ)</span> </label>
				<label class="col-sm-8 control-label" ><?php echo $info["detail"]; ?></label>									
			</div>
			<div class="form-group row">
				<label class="col-sm-3 control-label"><span>วันที่</span> &nbsp;:&nbsp; <?php echo revert_date($info["date_start"], true); ?> &nbsp;<span>น.</span></label>
				<label class="col-sm-3 control-label"><span>ถึงวันที่</span> &nbsp;:&nbsp; <?php echo revert_date($info["date_stop"], true); ?> &nbsp;<span>น.</span></label>
				<?php if($info["date_return"]!="" && $info["date_return"]!="0000-00-00 00:00:00") { ?>
				<label class="col-sm-3 control-label"><span>วันที่คืน</span> &nbsp;:&nbsp; <?php echo revert_date($info["date_return"], true); ?> &nbsp;<span>น.</span></label>
				<?php } ?>
				<label class="col-sm-3 control-label"><span>สถานะ</span>  &nbsp;:&nbsp; <?php echo $info["requeststatus_name"]; ?></label>							          
			</div> 

			<div class="form-group row"> 
				<label class="col-sm-2 control-label"><span>เก็บค่าบริการจาก </span></label>
				<div class="col-sm-10">
					<label class="col-sm-12 control-label"><?php echo ($info["servicecharge_id"]=="1") ? "&#9745;" : "&#9744;"; ?> งานส่วนกลางของคณะฯ</label>
					<label class="col-sm-12 control-label"><?php echo ($info["servicecharge_id"]=="2") ? "&#9745;" : "&#9744;"; ?> จ่ายเงินสด (โดยเจ้าของนำเงินเข้าเลขที่บัญชีเลขที่ 016-3-00325-6 ของคณะวิทยาศาสตร์ ม.มหิดล)</label>					
					<label class="col-sm-12 control-label"><?php echo ($info["servicecharge_id"]=="3") ? "&#9745;" : "&#9744;"; ?> <span>งานภาควิชา / หน่วยงาน</span> <?php echo $v["servicecharge_detail"]; ?></label>
				</div>

			</div>
			<?php if($info["requeststatus_id"]!=1 && $info["requeststatus_id"]!=4) { ?>	
			<div class="form-group row">
				<div class="col-sm-12">										
					<table >
						<thead>
							<tr style="font-weight:bold;">
								<th width="50%" class="center">เจ้าหน้าที่ ผู้ให้บริการ</th>
								<th width="50%" class="center">เจ้าหน้าที่ รับคืน</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="row">
										<div class="col-sm-12 center"><?php echo $info["requestname"]; ?>&nbsp;</div>
										<div class="col-sm-12 center"><?php echo revert_date($info["requestapprovetime"], true); ?>&nbsp;</div>
									</div>
								</td>												
								<td  class="center">
									<div class="row">
										<div class="col-sm-12 center"><?php echo $info["returnname"]; ?>&nbsp;</div>
										<div class="col-sm-12 center"><?php echo revert_date($info["returnapprovetime"], true); ?>&nbsp;</div>
									</div>
								</td>																								
							</tr>
						</tbody>
					</table>								
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<form class="form-horizontal" id="frmMain" method="post" action="saveExcel.php">
	<input type="hidden" name="filename" id="filename" value="<?php echo $info["docno"]; ?> ">
	<input type="hidden" name="dataHTML" id="dataHTML"></form>
</body>
<?php include_once ('inc/js-script.php'); ?>

<script type="text/javascript">
	function printPage(){
		   window.print();
		   setTimeout(" parent.$.fancybox.close()",1000);
		}

	function exportToExcel(){
		var t = $("body #page").html();
		$("#dataHTML").val(t);
		$("#frmMain").attr("action","saveExcel.php");
		$("#frmMain").submit();
	}

	function exportToword(){
		var t = $("body #page").html();
		$("#dataHTML").val(t);
		$("#frmMain").attr("action","saveDoc.php");
		$("#frmMain").submit();
	}
</script>
</html>