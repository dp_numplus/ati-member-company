<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
global $db;
$news_anchor_id = $_GET["news_anchor_id"];
$section = datatype(" and a.active='T'", "section", true);
$news_anchortype = datatype(" and a.active='T'", "news_anchortype", true);
$q = "select  name from news_anchor where news_anchor_id=$news_anchor_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["name"];
}else{
	$str = "เพิ่มประกาศ";
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">							
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-news-anchor.php">
						  <input type="hidden" name="news_anchor_id" id="news_anchor_id" value="<?php echo $news_anchor_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						  <div class="col-sm-12">		                
								<div class="form-group row">
								<label class="col-sm-1 control-label">รหัส</label>
								<div class="col-sm-2">
								  <input class="form-control" id="code" name="code" required="" placeholder="รหัส" type="text">
								</div>		                			                
								<label class="col-sm-1 control-label">หัวข้อ</label>
								<div class="col-sm-4">
								  <input class="form-control" name="name" id="name" required="" placeholder="หัวข้อประกาศ" type="text">
								</div>			                			                
								<label class="col-sm-2 control-label">ไฮไลท์</label>
								<div class="col-sm-2">
								  	<select name="highlight" id="highlight" class="form-control required">
									  <option selected="selected" value="T">Enable</option>
									  <option value="F">Disable</option>
									</select>
								</div>
							  </div>				                
								<div class="form-group row">	                			                
								<label class="col-sm-1 control-label">ลิงค์</label>
								<div class="col-sm-7">
								  <input class="form-control" name="link_to" id="link_to"  placeholder="ลิงค์ไปเว็บอื่น" type="text">
								</div>			                			                
									<label class="col-sm-2 control-label">สถานะ</label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
										  <option selected="selected" value="T">active</option>
										  <option value="F">inActive</option>
										</select>
									</div>
							  </div>
							  <div class="form-group row">
							  <label class="col-sm-1 control-label">รายละเอียด</label>
							  <div class="clear"></div><br>
							  	<div class="col-sm-12">
							  		<textarea id="detail" class="summernote" name="detail" placeholder="Description"></textarea>
							  	</div>
							  </div>
						  </div>
						  <div class="clear"></div>
						  <div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
						  </div>
						</form>
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>

<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var news_anchor_id = "<?php echo $_GET["news_anchor_id"]?>";
   if(news_anchor_id) viewInfo(news_anchor_id);
   $('#detail').summernote({
            height: 200,
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        }); 
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);//You can append as many data as you want. Check mozilla docs for this
            $.ajax({
                data: data,
                type: "POST",
                url: 'data/upload-img.php',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                	var url = "news_anchorImg/"+url;
                    editor.insertImage(welEditable, url);
                }
            });
        }

        
 });
 function viewInfo(news_anchor_id){
   if(typeof news_anchor_id=="undefined") return;
   getnews_anchorInfo(news_anchor_id);
}

function getnews_anchorInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/news-anchorlist.php";
	var param = "news_anchor_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}


function ckSave(id){
  onCkForm("#frmMain");
 var tmp = $('#detail').code();
 $('#detail').val(tmp);
  $("#frmMain").submit();
}	
</script>