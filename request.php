<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

if($_GET["type"]=="info"){
	include_once ('requestinfo.php');
}elseif($_GET["type"]=="detail"){
	include_once ('requestdetail.php');
}elseif($_GET["type"]=="childdetail"){
	include_once 'request-childdetail.php';
}else if($_GET["type"]=="discount"){
	include_once 'request-discount.php';
}else if($_GET["type"]=="discount-detail"){
	include_once 'request-discount-detail.php';
}else{
	include_once 'requestlist.php';
}
?>
