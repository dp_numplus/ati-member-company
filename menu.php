<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-5">
				<div class="content block-flat ">
					<div class="header" style="border-bottom:none;">							
						<h3><i class="fa fa-list"></i> &nbsp;Main Menu</h3>
					</div> 
					<table id="tbMenu" class="table" style="width:100%">
						<thead>
							<tr class="no-border">
								<th width="8%">No</th>
								<th width="9%">Code</th>
								<th width="23%">Entries</th>
								<th width="26%">Entries Eng</th>
								<th width="10%">Status</th>
								<th width="24%">Manage</th>
							</tr>
						</thead>   
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-sm-7">
				<div class="content block-flat ">
					<div class="header">							
						<h3><i class="fa fa-edit"></i> &nbsp;Menu Detail <span id="showname"></span></h3>
					</div>					
					<div class="content">
						<form class="form-horizontal" id="frmMain" method="post" action="update-menu.php">
							<input type="hidden" name="menu_id">
							<fieldset>
								<div class="form-group">
									<label class="col-sm-1 control-label">Code</label>
									<div class="col-sm-2">
										<input name="code" class="form-control" required placeholder=" " type="text">
									</div>			                			                
									<label class="col-sm-2 control-label">Name</label>
									<div class="col-sm-7">
										<input class="form-control" name="name" required placeholder=" " type="text">
									</div>			                			                
								</div>
								<div class="form-group">
									<label class="col-sm-1 control-label">Icon</label>
									<div class="col-sm-2">
										<input class="form-control" name="icon"  placeholder=" " type="text">
									</div>
									<label class="col-sm-2 control-label">Name Eng</label>
									<div class="col-sm-7">
										<input class="form-control" name="name_eng" placeholder=" " type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-1 control-label">File</label>
									<div class="col-sm-4">
										<input class="form-control" name="url" required placeholder=" " type="text">
									</div>
									<label class="col-sm-2 control-label">Class</label>
									<div class="col-sm-4">
										<input class="form-control"  name="use_class" placeholder=" " type="text">
									</div>
								</div>								
								<div class="form-group">
									<label class="col-sm-1 control-label">Action</label>
									<div class="col-sm-3">
										<input class="form-control" name="action" required placeholder=" " type="text">
									</div>
									<label class="col-sm-3 control-label">Status</label>
									<div class="col-sm-4">
										<select name="active" class="form-control" id="active" class="required">
											<option selected="selected" value="T">active</option>
											<option value="F">inActive</option>
										</select>
									</div>
								</div>
								<div class="content">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</fieldset>
						</form>					
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#frmMain").validate();
		listItem();
		var menu_id = "<?php echo $_GET["menu_id"]?>";
		if(menu_id) editInfo(menu_id);
	});
	function listItem(){
		var url = "data/menu.php";
		getDataTableNoExport(url, "#tbMenu");
	}
	function editInfo(id){
		if(typeof id=="undefined") return;
		var url = "data/menu.php";
		var param = "menu_id="+id;
		param = param+"&single=T";
		dataUrl(url, param,"#frmMain");
	}
	function ckSave(id){
		onCkForm("#frmMain");
		$("#frmMain").submit();
	}
</script>