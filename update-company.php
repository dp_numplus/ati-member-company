<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;
if (!empty($_FILES)) {
	// เริ่มต้นใช้งาน class.upload.php ด้วยการสร้าง instant จากคลาส
	$upload_image = new upload($_FILES['map_img']) ; // $_FILES['image_name'] ชื่อของช่องที่ให้เลือกไฟล์เพื่ออัปโหลด 
	//  ถ้าหากมีภาพถูกอัปโหลดมาจริง
	if ( $upload_image->uploaded ) { 
		// ย่อขนาดภาพให้เล็กลงหน่อย  โดยยึดขนาดภาพตามความกว้าง  ความสูงให้คำณวนอัตโนมัติ
		// ถ้าหากไม่ต้องการย่อขนาดภาพ ก็ลบ 3 บรรทัดด้านล่างทิ้งไปได้เลย
		$upload_image->file_src_name_body = "MAP-".$_POST["code"]."-".time();
		$upload_image->image_resize         = true ; // อนุญาติให้ย่อภาพได้
		$upload_image->image_x              = 800 ; // กำหนดความกว้างภาพเท่ากับ 400 pixel 
		$upload_image->image_ratio_y        = true; // ให้คำณวนความสูงอัตโนมัติ

		$upload_image->process( "mapImg/" ); // เก็บภาพไว้ในโฟลเดอร์ที่ต้องการ  *** โฟลเดอร์ต้องมี permission 0777

		// ถ้าหากว่าการจัดเก็บรูปภาพไม่มีปัญหา  เก็บชื่อภาพไว้ในตัวแปร เพื่อเอาไปเก็บในฐานข้อมูลต่อไป
		if ( $upload_image->processed ) {

			$image_name =  $upload_image->file_dst_name; // ชื่อไฟล์หลังกระบวนการเก็บ จะอยู่ที่ file_dst_name
			$upload_image->clean(); // คืนค่าหน่วยความจำ

		}// END if ( $upload_image->processed )

	}
}

/*print_r($_FILES);
echo "<hr>";
print_r($_POST);
die();*/
if($_POST){
    if($_POST["delimg"]=="T"){
	   if(file_exists($_POST["tmpimg"]))
	      unlink($_POST["tmpimg"]);
	   $_POST["tmpimg"] = "";
    }
	$args = array();
	$args["table"] = "company";
	if($_POST["company_id"])
	   $args["id"] = $_POST["company_id"];
	$args["code"] = $_POST["code"];
	$args["name"] = $_POST["name"];
	$args["name_eng"] = $_POST["name_eng"];
	$args["email"] = $_POST["email"];
	$args["address"] = $_POST["address"];
	$args["phone"] = $_POST["phone"];
	$args["tax_no"] = $_POST["tax_no"];
	$args["fax"] = $_POST["fax"];
	$args["website"] = $db->escape($_POST["website"]);
	$args["tax_address"] = $_POST["tax_address"];
	$args["branch"] = $_POST["branch"];
	$args["recby_id"] = (int) $EMPID;
	$args["capital"] = $_POST["capital"];
	$args["ratio"] = $_POST["ratio"];
	$args["active"] = $_POST["active"];
	$args["rectime"] = date("Y-m-d H:i:s");
	$args["map_img"] = $image_name ? "mapImg/$image_name" : $_POST["tmpimg"];
   $ret = $db->set($args);
   $company_id = $args["id"] ? $args["id"] : $ret;
}

$args = array();
$args["p"] = "company";
$args["company_id"] = $company_id;
$args["type"] = "info";
redirect_url($args);
?>