<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>
	<?php
	include_once "./lib/lib.php";
	include_once "./connection/connection.php";
	include_once "./share/datatype.php";
	global $db;
	$news_id = $_GET["news_id"];
	$newstype = datatype(" and a.active='T'", "newstype", true);
	$q = "select  name from news where news_id=$news_id";
	$r = $db->rows($q);
	$str = "";
	if($r){
		$str = $r["name"];
	}else{
		$str = "เพิ่มคลังความรู้";
	}
	?>
	<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
	<div id="cl-wrapper">
		<div class="container-fluid" id="pcont">
			<div class="cl-mcont" style="height:400px;">
				
					<div class="col-md-12">
						<div class="content">
							<div class="cl-mcont">
								<form action="update-upload.php" class="dropzone dz-clickable" id="my-awesome-dropzone">
									<input type="hidden" name="news_id" id="news_id" value="<?php echo $news_id; ?>">
									<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
								</form>
							</div>						
						    <a class="label label-default pull-right" href="#" onclick="ckClose();">ปิดหน้านี้</a>		
						</div>


					</div>
				
			</div>
		</div> 
	</div>
	<?php 		  	
	include_once ('inc/js-script.php');
	include ('inc/footer.php') ?>
</body>

<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>
<script type="text/javascript" src="js/dropzone/dropzone.js"></script>
<script type="text/javascript">

function ckClose(){
		parent.reView();
		parent.$.fancybox.close();
	}
</script>
</html>
<?php $db->disconnect(); ?>