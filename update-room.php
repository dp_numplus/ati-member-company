<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;

$code = trim($_POST["code"]);
$dup = 0;
$id = "";
/*if($code && $_POST["active"]!='F'){
	$id = $_POST["room_id"];
	$con = ($id) ? " and room_id != $id" : "";
	$q = "select count(room_id) as c from room where code='$code' $con and active!='F'";
	$rs = $db->data($q);
	if($rs>0) $dup = 1;
}
if(!$code || $dup==1){
	$args = array();
	$args["p"] = "room";
	$args["room_id"] = $id;
	$args["type"] = "info";
	$_SESSION["error"]["msg"] = "รหัส $code ซ้ำ";
	redirect_url($args);
}
*/
if($_POST){
	$args = array();
	$args["table"] = "room";
	if($_POST["room_id"]){
	   $args["id"] = $_POST["room_id"];
	}else{
		$args["roomstatus_id"] = 1;
	}
	$args["code"] = $_POST["code"];
	$args["roomtype_id"] = (int)$_POST["roomtype_id"];
	$args["name"] = $_POST["name"];
	$args["amount"] = $_POST["amount"];
	$args["remark"] = $_POST["remark"];
	$args["active"] = $_POST["active"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	
   $ret = $db->set($args);
   $room_id = $args["id"] ? $args["id"] : $ret;
}
$_SESSION["success"]["msg"] = "บันทึกข้อมูลเรียบร้อยแล้ว";

$args = array();
$args["p"] = "room";
$args["room_id"] = $room_id;
$args["type"] = "info";
redirect_url($args);
?>