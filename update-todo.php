<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;
if($_POST){
	$args = array();
	$args["table"] = "todo";
	if($_POST["todo_id"]){
	   $args["id"] = $_POST["todo_id"];
	}	else{
		$args["created_date"] = date("Y-m-d H:i:s");
		$args["createdby_id"] = (int)$EMPID;
	}
	$args["code"] = $_POST["code"];
	$args["company_id"] = (int)$_POST["company_id"];
	$args["assign_to_id"] = (int)$_POST["assign_to_id"];
	$args["todostatus_id"] = (int)$_POST["todostatus_id"];
	$args["name"] = $_POST["name"];
	$args["detail"] = $_POST["detail"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	$args["target_date"] = thai_to_timestamp($_POST["target_date"]);
   $ret = $db->set($args);
   $todo_id = $args["id"] ? $args["id"] : $ret;

}
$args = array();
$args["p"] = "todo";
$args["todo_id"] = $todo_id;
$args["type"] = "info";
redirect_url($args);
?>