<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;
if($_POST){
	$args = array();
	$args["table"] = "news";
	if($_POST["news_id"]){
	   $args["id"] = $_POST["news_id"];
	}	else{
		$args["created_date"] = date("Y-m-d H:i:s");
		$args["createdby_id"] = (int)$EMPID;
	}

	$args["code"] = $_POST["code"];
	$args["quotation_no"] = $_POST["quotation_no"];
	$args["newstype_id"] = (int)$_POST["newstype_id"];
	$args["company_id"] = (int)$_POST["company_id"];
	$args["assign_to_id"] = (int)$_POST["assign_to_id"];
	$args["newsstatus_id"] = (int)$_POST["newsstatus_id"];
	$args["name"] = $_POST["name"];
	$args["detail"] = $_POST["detail"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	$args["target_date"] = thai_to_timestamp($_POST["target_date"]);
   $ret = $db->set($args);
   $news_id = $args["id"] ? $args["id"] : $ret;

}
$args = array();
$args["p"] = "company";
$args["news_id"] = $news_id;
$args["newtype_id"] = $_POST["newstype_id"];
$args["company_id"] = $_POST["company_id"];
$args["type"] = "newsinfo";
redirect_url($args);
?>