<?php 
$msg = $_SESSION["login"]["msg"];
unset($_SESSION["login"]["msg"]);
?>
<div id="cl-wrapper" class="login-container">
	<div class="middle-login" style="max-width: 500px;">
		<div class="block-flat">
			<div>
				<form style="margin-bottom: 0px !important;" class="form-horizontal" method="post" action="update-login.php">
					<div class="content" style=" padding-top: 5px">
					
							<h3 class="text-center"><img class="logo-img" src="images/logo-fa.png" alt="logo" style="max-width: 100%;" /></h3>						
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="text" placeholder="Username" id="username" name="username" class="form-control">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" placeholder="Password" id="password" name="password" class="form-control">
									</div>
								</div>
							</div>
							
					</div>
					<div class="foot">
					
						<a class="btn btn-default" data-dismiss="modal" type="button" href="../index.php">หน้าหลัก</a>
						<button class="btn btn-primary" data-dismiss="modal" type="submit">เข้าสู่ระบบ</button>
					</div>
				</form>
			</div>
		</div>
	</div> 
</div>

<?php include ('inc/js-script.php') ?>

<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$("#form_login").validate();
	});
	var msg = "<?php echo $msg?>";
	if(msg!=""){
		var options =  $.parseJSON('{"text":"<p>'+msg+'</p>","layout":"center","type":"error"}');
		noty(options); 
	}
</script>