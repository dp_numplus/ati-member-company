<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/person.php";
global $db;
global $SECTIONID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$person_id = $_GET["person_id"];
$company_id = $_GET["company_id"];
$company = datatype(" and a.active='T'", "company", true);
$q = "select name, phone from company where company_id=$company_id";
$company_data = $db->rows($q);
$select = $company_data["name"];
$company_phone = $company_data["phone"];
$q = "select  name from person where person_id=$person_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["name"];
	$company_phone = "";
}else{
	$str = "เพิ่มบุคลากร";
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont" style="width:1280px; margin:0 auto;">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">Home</a></li>
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=info&company_id=<?php echo $company_id ?>');"><?php echo $select; ?></a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-person.php">
							<input type="hidden" name="person_id" id="person_id" value="<?php echo $person_id; ?>">
							<input type="hidden" name="company_id" id="company_id" value="<?php echo $company_id; ?>">
							<div class="col-sm-12">                      
								<div class="form-group row"> 
                                      
									<label class="col-sm-2 control-label">ชื่อ-นามสกุล <span class="red" style="display:inline-block; position:absolute;"> &nbsp;*</span></label>
									<div class="col-sm-3">
										<input class="form-control" name="name" id="name" required="" placeholder="ชื่อ-นามสกุล" type="text">
									</div>                                            
									<label class="col-sm-1 control-label">ตำแหน่ง</label>
									<div class="col-sm-3">
										<input class="form-control" name="position" id="position"  placeholder="ตำแหน่ง" type="text">
									</div>
								</div> 
								 <div class="form-group row">
									<label class="col-sm-2 control-label">อีเมล</label>
									<div class="col-sm-2">
									  <input class="form-control" name="email" required="" id="email" placeholder="Enter a valid e-mail" type="email">
									</div>
<!-- 
									<label class="col-sm-1 control-label">เบอร์โทร</label>
									<div class="col-sm-2">
									  <input class="form-control" value="<?php echo $company_phone; ?>" required="" name="phone" id="phone" placeholder="เบอร์โทร" type="text">
									</div>
									 -->
<!-- 
									<label class="col-sm-1 control-label">เบอร์ต่อ</label>
									<div class="col-sm-2">
									  <input class="form-control"  name="ext" id="ext" placeholder="เบอร์ต่อ" type="text">
									</div>
									 -->
									<label class="col-sm-2 control-label">หมายเลขโทรศัพท์</label>
									<div class="col-sm-2">
									  <input class="form-control" name="mobile" id="mobile" placeholder="หมายเลขโทรศัพท์">
									</div>
								  </div>
								<div class="form-group row">
								  <label class="col-sm-2 control-label">หมายเหตุ</label>						
								  	<div class="col-sm-6">
								  		<textarea id="remark" class="form-control" name="remark" placeholder="หมายเหตุ"></textarea>
								  	</div>
							  </div>                       
								<div class="form-group row"> 
									<label class="col-sm-2 control-label">สถานะ <span class="red">*</span></label>
									<div class="col-sm-2">
										<select name="main" id="main" class="form-control required">
											<option selected="selected" value="T">Main</option>
											<option value="F">General</option>
										</select>
									</div>
								</div>								
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	 $("#frmMain").validate();
	 var person_id = "<?php echo $_GET["person_id"]?>";
	 if(person_id) viewInfo(person_id);
	 //$('#remark').summernote({height: 80});
     var error = "<?php echo $error ?>";
     if(error!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: error,
		        class_name: 'danger'
		      });
	        }
	     });
     }
     var success = "<?php echo $success ?>";
     if(success!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: success,
		        class_name: 'success'
		      });
	        }
	     });
     }


 });
var trMenuList = $("#tbMenuList tbody tr:eq(0)").clone();
delRow("#tbMenuList");
 function viewInfo(person_id){
	 if(typeof person_id=="undefined") return;
	 getInfo(person_id);
}

function getInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/personlist.php";
	var param = "person_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");

}

function ckSave(id){
	onCkForm("#frmMain");
	//var tmp = $('#remark').code();
	//$('#remark').val(tmp);
	$("#frmMain").submit();
} 
</script>