<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$msg = $_SESSION["popup"]["msg"];
unset($_SESSION["popup"]["msg"]);
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-3">
				<div class="content block-flat ">
					<div class="header" style="border-bottom:none;">							
						<h3><i class="fa fa-list"></i> &nbsp;Menu Access</h3>
					</div> 
					<table id="tbMenu"style="width:100%">
						<thead>
							<tr>
								<th width="6%">No</th>
								<th width="94%">List</th>
							</tr>
						</thead>  
						<tbody>
							<?php
							$i = 1;
							$id = trim($_GET["con_id"],",");
							$con = $id ? " and righttype_id in ($id)" : ""; 
							$q = "select * from righttype where active='T' $con";
							$array_right = $db->get($q);
							if(is_array($array_right)){
								foreach($array_right as $k=>$v){
									?> 
									<tr onClick="singleKey(this)" style="cursor:pointer;">
										<td><?php echo $i;?>.<input name="pId" id="pId" type="hidden" value="<?php echo $v["righttype_id"];?>">&nbsp;<input name="table_name" id="table_name" type="hidden" value="<?php echo $v["table_name"];?>"></td>
											<td id="name"><?php echo $v["name"];?>&nbsp;</td>							    
										</tr>
										<?php
										$i++;
									} 
								}?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-sm-9">
					<div class="content block-flat ">
						      <form action="update-menu-access.php" class="form-horizontal" id="frmMain" method="post" >
								     <fieldset><input name="righttype_id" id="righttype_id" type="hidden" >
								      <div class="header">							
											<h3><i class="fa fa-edit"></i> Setting <span id="righttypename"></span></h3>
											<br>
										</div>	
								    <div class="row-fluid" id="boxContent">
								          <div class="col-md-12">

								        <div class="box-content" style="margin-top:15px;">
								         <div class="header">
								              
								             <h3><i class="icon-edit"></i>&nbsp;Menu</h3> 
								              
								            </div>
								              <table  class="no-border" id="tbList">
								            <thead>
								                  <tr>
								                <th width="27%">No</th>
								                <th width="73%">Menu Name</th>
								              </tr>
								                </thead>
								            <tbody>
								                  <tr>
								                <td><input name="ckMenu[]" type="checkbox" checked id="ckMenu" value="T">
								                      <span id="runNo"></span>
								                      <input name="menu_id[]" type="hidden" id="menu_id">
								                      <input name="menuvisible_id[]" type="hidden" id="menuvisible_id"></td>
								                <td class="center" id="menuname">&nbsp;</td>
								              </tr>
								                </tbody>
								          </table>
								          <br>
								              <button type="button" class="btn" onClick="addMenu();">Add Menu</button>
								            </div>
								            <hr>
								       
								        <div class="box-content" style="margin-top:15px;">
								                <div class="header">
								              
								             <h3><i class="icon-edit"></i>&nbsp;Menu List</h3> 
								              
								            </div> 
								              <table  class="no-border" id="tbMenuList">
								            <thead>
								                  <tr>
								                <th width="27%">No</th>
								                <th width="73%">Menu List Name</th>
								              </tr>
								                </thead>
								            <tbody>
								                  <tr>
								                <td><input name="ckMenuList[]" type="checkbox" checked id="ckMenuList" value="T">
								                      <span id="runNo"></span>
								                      <input name="menulist_id[]" type="hidden" id="menulist_id">
								                      <input name="menuvisiblelist_id[]" type="hidden" id="menuvisiblelist_id"></td>
								                <td class="center" id="menulistname">&nbsp;</td>
								              </tr>
								                </tbody>
								          </table>
								          <br>
								              <button type="button" class="btn" onClick="addMenuList();">Add Menu List</button>
								            </div>
								      </div>
								          <!--/span--> 
								        </div>
								        <div class="clear"></div>
								        <hr>
								<div class=" col-md-12" style="margin-top:15px;">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
								        </fieldset>
								  </form>
						</div>
					</div>
				</div>
			</div> 
		</div>

		<?php include ('inc/js-script.php') ?>
		
	<script type="text/javascript">
		$(document).ready(function() {
			$("#frmMain").validate();
			var righttype_id = "<?php echo $_GET["righttype_id"]?>";
			var tmpName = "<?php echo $_GET["righttypename"]?>";
			if(righttype_id) viewInfo(righttype_id, tmpName);
		});
		var trList = $("#tbList tbody tr:eq(0)").clone();
		delRow("#tbList");
		var trMenuList = $("#tbMenuList tbody tr:eq(0)").clone();
		delRow("#tbMenuList");

		function retMenuInfo(id){
			//console.log(id);
			for(var i in id){
				var ck = "";
				var data = id[i];
				data["menu_id"] = data["id"];
				data["menuname"] = data["name"];
		       $("#tbList tbody tr ").each(function(){
		            var t = $(this).find("input#menu_id").val();
					var ckId = data["id"];
					if(ckId==t){
						ck = "1";
					}
		 		});
		 	   if(ck=="1"){
		 	 	   ck = "";
		 	 	   continue;
		 	   }
			   var t = addTrLine("#tbList", trList, data, "runNo");
			}
		}

		function retMenuListInfo(id){
			for(var i in id){
				var ck = "";
				var data = id[i];
				data["menulist_id"] = data["id"];
				data["menulistname"] = data["name"];
		       $("#tbMenuList tbody tr ").each(function(){
		             var t = $(this).find("input#menulist_id").val();
		 				 var ckId = data["id"];
		 				 if(ckId==t){
		 				    ck = "1";
		 				 }
		 			 });
		 	   if(ck=="1"){
		 	 	   ck = "";
		 	 	   continue;
		 	   }
			   var t = addTrLine("#tbMenuList", trMenuList, data, "runNo");
			}
		}

		function dispMenu(id){
			delRow("#tbList");
		   var url = "data/menuvisible.php";
			var param = "righttype_id="+id+"&type=menu";
			$.ajax( {
				"dataType":'json', 
				"type": "POST", 
				"url": url,
				"data": param, 
				"success": function(data){	
					 $.each(data, function(index, array){
						 array.name = array.menuname;
		             addTrLine("#tbList", trList, array, "runNo");    
					 });				 
				}
			});
		}

		function dispMenuList(id){
			delRow("#tbMenuList");
		   var url = "data/menuvisible.php";
			var param = "righttype_id="+id+"&type=menulist";
			$.ajax( {
				"dataType":'json', 
				"type": "POST", 
				"url": url,
				"data": param, 
				"success": function(data){	
					 $.each(data, function(index, array){
						 array.name = array.menulistname;
						 array.menuvisiblelist_id = array.menuvisible_id;
		             addTrLine("#tbMenuList", trMenuList, array, "runNo");    
					 });				 
				}
			});
		}

		function viewInfo(id, tmpName){
			if(typeof id=="undefined") return;
			   $("#frmMain input[name=righttype_id]").val(id);
			   $("#frmMain input[name=righttypename]").val(tmpName);
			   dispMenu(id);
			   dispMenuList(id);
		    $('html, body').animate({
		        scrollTop: $("#boxContent").offset().top
		    }, 600);
		}

		function addMenu(){
			if($("#righttype_id").val()=="")return;
		   var url = "menu map=code,name";
		   selectData(url,"retMenuInfo", 1);	
		}

		function addMenuList(){
			if($("#righttype_id").val()=="")return;
			var strCon = "";
			 $("#tbList tbody tr").each(function(){
			  var id = $(this).find('#menu_id').val();
		       if(id>0){
				   strCon = strCon+id+",";
				   
				}
		     });
		   var url = "menulist map=code,name con=menu_id-"+strCon;
		   selectData(url,"retMenuListInfo", 1);	
		}

		function singleKey(tag){
		   var id = $(tag).find('#pId').val();	
		   var code = $(tag).find('#code').text();
		   var name = $(tag).find('#name').text();
		   $("#frmMain input[name=righttype_id]").val(id);
		   $("#frmMain #righttypename").html(name);
		   dispMenu(id);
		   dispMenuList(id);
		}

		function ckSave(id){
		  if($("#righttype_id").val()=="")return;
		  onCkForm("#frmMain");  
		  $("#frmMain").submit();
		}

	</script>	