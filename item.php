<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

if($_GET["type"]=="info"){
	include_once ('iteminfo.php');
}elseif($_GET["type"]=="detail"){
	include_once ('itemdetail.php');
}elseif($_GET["type"]=="childdetail"){
	include_once 'item-childdetail.php';
}else if($_GET["type"]=="discount"){
	include_once 'item-discount.php';
}else if($_GET["type"]=="discount-detail"){
	include_once 'item-discount-detail.php';
}else{
	include_once 'itemlist.php';
}
?>
