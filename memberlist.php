<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$university = datatype(" and a.active='T'", "university", true);
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มการสมัครทำงาน</button>
						<h3><i class="fa fa-list"></i> &nbsp;การสมัครทำงาน</h3>
					</div>
						<div class="header">
							<div class="form-group row">             
								<label class="col-sm-2 control-label">ชื่อสถาบันการศึกษา<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="university_id" id="university_id" class="select2" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($university as $key => $value) {
											$id = $value['university_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>             
								<label class="col-sm-2 control-label">ระดับชั้นที่กำลังศึกษา<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="educationlevel_id" id="educationlevel_id" class="select2" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($educationlevel as $key => $value) {
											$id = $value['educationlevel_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label">สถานะ</label>
								<div class="col-sm-2">
									<select name="active" id="active" class="form-control" onchange="reCall();">
										<option selected="selected" value="T">active</option>
										<option value="F">inActive</option>
									</select>
								</div>                                           
							</div> 
						</div>
					<table id="tbMember" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="5%">ลำดับ</th>
								  <th width="6%">คำนำหน้า</th>
								  <th width="15%">ชื่อ</th>
								  <th width="15%">นามสกุล</th>
								  <th width="8%">ชื่อเล่น</th>
								  <th width="10%">Email</th>
								  <th width="10%">เบอร์โทร</th>
								  <th width="10%">Line</th>
								  <th width="11%">รหัสประจำตัวประชาชน</th>
								  <th width="8%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
			<div class="clear"></div>
				</div>
			</div>
		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	listItem();
});

function listItem(){
   var url = "data/memberlist.php";
   oTable = $("#tbMember").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfomemberty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"university_id","value":$("#university_id").val()});
			aoData.push({"name":"educationlevel_id","value":$("#educationlevel_id").val()});
			aoData.push({"name":"active","value":$("#active").val()});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&member_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

</script>