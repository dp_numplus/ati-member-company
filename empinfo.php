<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$section = datatype(" and a.active='T'", "section", true);
$emp_id = $_GET["emp_id"];
$q = "select  prefix, fname, lname from emp where emp_id=$emp_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["prefix"]." ".$r["fname"]." ".$r["lname"];
}else{
	$str = "Add New";
}
?>
<div id="cl-wrapper">
	<div class="container" id="pcont">
		<div class="cl-mcont" style="width:1280px; margin:0 auto;">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">Home</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>							
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-emp.php">
						  <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						
							  <div class="form-group row">

								<label class="col-sm-2 control-label">คำนำหน้าชื่อ</label>
								<div class="col-sm-2">
								  <input class="form-control" id="prefix" name="prefix" placeholder="คำนำหน้าชื่อ" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label">ชื่อ (TH) </label>
								<div class="col-sm-2">
								  <input class="form-control" name="fname" id="fname" required="" placeholder="ชื่อ" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label">นามสกุล (TH)</label>
								<div class="col-sm-2">
								  <input class="form-control" required="" name="lname" id="lname" placeholder="นามสกุล" type="text">
								</div>
							  </div>
							  <div class="form-group row">
							  	<label class="col-sm-2 control-label">สังกัดบริษัท</label>
								<div class="col-sm-2">
								  <input class="form-control" name="org_name" id="org_name" placeholder="" type="text">
								</div>
								<label class="col-sm-1 control-label">ตำแหน่ง</label>
								<div class="col-sm-2">
								  <input class="form-control" name="position" id="position" placeholder="" type="text">
								</div>
							  </div>
							  <div class="form-group row">
 								<label class="col-sm-2 control-label">หมายเลขโทรศัพท์</label>
								<div class="col-sm-2">
								  <input class="form-control" name="phone" id="phone" placeholder="หมายเลขโทรศัพท์" type="text">
								</div>
								<label class="col-sm-1 control-label">E-Mail</label>
								<div class="col-sm-2">
								  <input class="form-control" name="email" id="email" placeholder="E-Mail" type="email">
								</div>
									<label class="col-sm-1 control-label">Status</label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
										  <option selected="selected" value="T">เปิดใช้งาน</option>
										  <option value="F">ไม่ใช้งาน</option>
										</select>
									</div>
							  </div>
						  <div class="form-group row">
								<label class="col-sm-2 control-label">Username</label>
								<div class="col-sm-2">
								  <input class="form-control" name="username" id="username" placeholder="username" type="text">
								</div>			                
								<label class="col-sm-1 control-label">Password</label>
								<div class="col-sm-2">
								  <input class="form-control" name="password" id="password" placeholder="password" type="password">
								</div>
								
						  </div>
						  <div class="form-group row">
						  	<label class="col-sm-2 control-label">สิทธิ์ (Group)</label>
								<div class="col-sm-2">

								  <select name="righttype_id"  id="righttype_id" class="form-control required">
					                <?php
					                $q = "select righttype_id, name from righttype where active='T'";
					                $r = $db->get($q);
					                foreach($r as $k=>$v){
					                 ?>
					                 <option  value="<?php echo $v["righttype_id"]?>"><?php echo $v["name"]?></option>
					                 <?php } ?>
					               </select>

								</div>
								<label class="col-sm-1 control-label">login Status</label>
								<div class="col-sm-2">
									<select name="activelogin" id="activelogin" class="form-control required">
									  <option selected="selected" value="T">เปิด</option>
									  <option value="F">ปิด</option>
									</select>
								</div>
						  </div>
							<div class="clear"></div>
							<br>
						  <div class="form-group">
								<div class="col-md-10 col-sm-offset-2 ">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
						  </div>
						</form>
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var emp_id = "<?php echo $_GET["emp_id"]?>";
   if(emp_id) viewInfo(emp_id);
   $("#birthdate").datepicker({language:'th-th',format:'dd-mm-yyyy'});
 });
function readURL(input) {
 if (input.files && input.files[0]) {
  var reader = new FileReader();
  reader.onload = function(e) {
   $('#img').attr('src', e.target.result);
 }
 reader.readAsDataURL(input.files[0]);
}
}
 function viewInfo(emp_id){
   if(typeof emp_id=="undefined") return;
   getEmpInfo(emp_id);
}

function getEmpInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/emplist.php";
	var param = "emp_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}

function removeImg(){
  var defaultImg = "images/no-avatar-male.jpg";
  if($('input[name=tmpimg]').val()=="")return;
  var t = confirm("ลบรูปภาพ");
  if(!t) return;
  $('#img').attr('src', defaultImg);
  $('input[name=delimg]').val("T");	
  ckSave();
}
function ckSave(id){
	var activelogin = $("#activelogin").val();
	 var email = $("#email").val();
	 var password = $("#password").val();
	 password = password.trim();
	 // alert(password);
	 if ( email && activelogin=='T' ) {
	 	if ( password=='' ) {
	 		$("#username").val(email);
	 		$("#password").val(email); 	
	 	}
	 }
  onCkForm("#frmMain");
  $("#frmMain").submit();
}	
</script>