<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/request.php";
include_once "../share/room.php";
global $db;
$sWhere = "";
$date_start = $_POST["date_start"];
$sWhere .= " and a.requeststatus_id=2";
$r = get_request($sWhere);
$list_room = "";
foreach($r as $k=>$v){
	$date = revert_date($v["date_start"]);
	if($date==$date_start){
		$list_room .= ",".$v["room_id"];
	}
	//$array_data[][$date] =  $v["room_id"];
}

$ids = trim($list_room, ",");
/*print_r($array_data);*/
$con_rom = " and a.active='T' ";
if($ids) $con_rom .= " and a.room_id not in ($ids)";
$room = datatype($con_rom, "room", true);
?>
<select name="room_id" id="room_id" class="select2" onchange="addrequest_detail();">
	<option value="">---- เลือก ----</option>
	<?php foreach ($room as $key => $value) {
		$id = $value['room_id'];
		$s = "";
		if($select==$id){
			$s = "selected";
		}
		$name = $value['name'];
		echo  "<option value='$id' {$s}>$name</option>";
	} ?>

</select>

<?php
die();
?>