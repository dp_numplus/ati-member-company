<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
global $db;

$q = "select a.name, a.url, a.news_upload_id from news_upload a where a.news_id=".$_POST["news_id"];
$r = $db->get($q);
if($r){ 
	$runing = 1;
	?>
	<div class="table-responsive">
		<table class="table no-border hover">
			<thead class="no-border">
				<tr>
					<th style="width:10%;">No</th>
					<th style="width:80%;"><strong>File</strong></th>
					<th style="width:10%;" class="text-center"><strong>จัดการ</strong></th>
				</tr>
			</thead>
			<tbody class="no-border-y">
			<?php foreach ($r as $k => $v) { ?>
			<tr>
				<td ><?php echo $runing; ?></td>
				<td ><a target="_black" href="<?php echo $v["url"].$v["name"]; ?>"><strong><?php echo $v["name"]; ?></strong></a> </td>
				<td class="text-center"><a  style="cursor:pointer;"class="label label-danger"  onclick="delete_file(<?php echo $v["news_upload_id"]; ?>)"><i class="fa fa-times"></i></a></td>
			</tr>				

			<?php
				$runing++;
			 }	?>
			</tbody>
		</table>		
	</div>
<?php } ?>
