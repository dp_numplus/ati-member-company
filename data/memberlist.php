<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
include_once "../share/emp.php";
global $db;
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["member_id"];
	if($id){
		$r = view_member("", $id);
		foreach($r as $k=>$v){
			$v["birthdate"] = revert_date($v["birthdate"]);
			if(!$v["img"])
				$v["img"] = "images/no-avatar-male.jpg";
			$con = " and a.member_id=$id";
			$t = login_info($con);
			if($t){
				foreach($t as $key=>$val){
					$v["activelogin"] = $val["active"];
					$v["righttype_id"] = $val["righttype_id"];
					$v["username"] = $val["username"];
					$v["password"] = $val["password"];
				}
			}else{
				$v["activelogin"] = "";
				$v["righttype_id"] = "";
				$v["username"] = "";
				$v["password"] = "";  	
			}
			$aData[] = $v;
		}
	}  
}else{
	$aColumns = array( 'member_Id','prefix','fname','lname','cid');
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "memberId";

	function fnColumnToField( $i ){
		/* Note that column 0 is the details column */
		if ( $i == 0 ||$i == 1 )
			return "member_id";
		else if ( $i == 2 )
			return "prefix";
		else if ( $i == 3 )
			return "fname";
		else if ( $i == 4 )
			return "lname";
		else if ( $i == 5 )
			return "cid";
	}

	$sLimit = "";
	if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
		$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
	}


	/* Ordering */
	if(isset($_POST['iSortCol_0'])){
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
			$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
			".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}

	/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE active!='' ";
  $sAND = "";
	if($_POST['sSearch'] != ""){
  		 $sWhere = " and ( fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			   	   "lname LIKE '%".$db->escape( $_POST['sSearch'] )."%' )";
	}
	$sWhere .= ($_POST["educationlevel_id"]) ? " and educationlevel_id={$_POST["educationlevel_id"]}" : "";
	$sWhere .= ($_POST["university_id"]) ? " and university_id={$_POST["university_id"]}" : "";	
	$sWhere .= ($_POST["active"]) ? " and active='{$_POST["active"]}'" : "";	
	/* Paging */
	$sQuery = "SELECT member_id, prefix, fname, lname, cid, email,phone, nickname, line
	FROM member
	$WHERE $sWhere
	$sOrder
	$sLimit";

	$rResult = $db->get($sQuery);
	$a = array();
	if(is_array($rResult)){
		$runNo = 1;
		foreach ($rResult as $r){
			$id = $r["member_id"]; 
			$manage =  get_datatable_icon("edit", $id);
			$active = ($r["active"]=="T") ? "active" : "nonActive";   
			$a[] = array($runNo
				,$r['prefix']
				,$r['fname']
				,$r['lname']
				,$r['nickname']
				,$r['email']
				,$r['phone']
				,$r['line']
				,$r["cid"]
				,$manage);
			$runNo++;
		}
	}

	$aData = array();
	$sQuery = "SELECT COUNT(*) as total
	FROM member
	$WHERE $sWhere";

	$rs = $db->data($sQuery);
	$iFilteredTotal = $rs;

	$sQuery = "SELECT COUNT(*) as total
	FROM member";
	$resultTotal = $db->data($sQuery);
	$iTotal = $resultTotal;

	$aData["sEcho"] = intval($_POST['sEcho']);
	$aData["iTotalRecords"] = $iTotal; 
	$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
	$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
