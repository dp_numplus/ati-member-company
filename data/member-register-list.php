<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
global $atiDB;

// d($_POST);

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["member_id"];
	if($id){
   $r = view_member("", $id);
    foreach ($r as $k => $v) {
    	
    	$v["select_university"] = $v["grd_ugrp1"]."-".$v["grd_uid1"];
    	$v["birthdate"] = revert_date($v["birthdate"]);
    	$v["password2"] = $v["password"]; 
    	if(!$v["img"])
			$v["img"] = "images/no-avatar-male.jpg";    
	    $aData[] = $v;	   }  
	}
}else{
  $aColumns = array( 'member_Id','title_th','fname_th','lname_th','cid');
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "memberId";

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "member_id";
	else if ( $i == 2 )
		return "cid";
	else if ( $i == 3 )
		return "fname_th";
	else if ( $i == 4 )
		return "fname_en";
	else if ( $i == 5 )
		return "reg_date";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$atiDB->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($atiDB->escape( $_POST['iSortCol_'.$i] ))."
                ".$atiDB->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
if ( !empty($_POST["member_company_sort"]) ) {
	$sOrder = "ORDER BY fname_th ASC";
}else{
	$sOrder = "ORDER BY member_id desc";
}// end else

/* Filtering */
$WHERE = "WHERE  active!='' ";
$sWhere = " ";
$sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "   ( title_th LIKE '%".$atiDB->escape( $_POST['sSearch'] )."%' OR ".
			    "fname_th LIKE '%".$atiDB->escape( $_POST['sSearch'] )."%' OR ".
			    "lname_th LIKE '%".$atiDB->escape( $_POST['sSearch'] )."%' OR ".			    
                "title_en LIKE '%".$atiDB->escape( $_POST['sSearch'] )."%' OR ".
			    "fname_en LIKE '%".$atiDB->escape( $_POST['sSearch'] )."%' OR ".
			    "lname_en LIKE '%".$atiDB->escape( $_POST['sSearch'] )."%' OR ".
			    "cid LIKE '%".$atiDB->escape( $_POST['sSearch'] )."%' )";
	$sAND = "AND ";
	if($_POST["fa_member"]=="T")
		$_POST["fa_member"] = "F";
	
}
$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and reg_date>='$dateStart 00:00:00' and reg_date<='$dateStop 23:59:59'" : "";
$sWhere .= ($_POST["fa_member"]=="T") ? " and active='T' and fa='T'" : "";
/* Paging */
$sQuery = "SELECT member_id
			, title_th
			, fname_th
			, title_th_text
			, title_en_text
			, lname_th
			, title_en
			, fname_en
			, lname_en
			, cid
			, reg_date
			, img
			, org_name
			, tel_home
			, email1
			, fa
			, private
           FROM member
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $atiDB->get($sQuery);

/*
	$str_txt = "";
	$str_txt .= "{$sQuery}";
	$log_name = "./log/memberlist.log";
	$file = fopen($log_name, 'w');
	$date_now = date('Y-m-d H:i:s');
	$str_txt = $date_now."|".$str_txt;
	fwrite($file, $str_txt);
	fclose($file);
*/

$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["member_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive"; 
	  $url = "index.php?p=mamber&type=register-history&member_id=".$r["member_id"];
	  $button = '<a class="btn btn-success" href="'.$url.'" target="_blank"><i class="fa fa-list"></i>ประวัติ/ลงทะเบียน </a>';   	  
	  $btn_reset_passwd = '<a class="btn btn-warning" title="Reset password" onClick="reset_passwd('.$r["member_id"].','.$r["cid"].')"><i class="fa fa-exclamation-triangle"></i></a>';
   	  $full_name = $r['title_th']." ".$r['fname_th']." ".$r['lname_th'];
   	  $part_img = "../../fa-backoffice/".$r["img"];
	  $full_name .= ($r["img"]!="") ? '&nbsp;<a  title="'.$full_name.'" rel="fancybox" href="'.$part_img.'"><i class="fa fa-picture-o"></i> ดูรูปภาพ</a>' : '';

		if ( ($r["title_en"]=='Other')||(!$r["title_en_text"]=='') ) {			
			$r['title_en'] = $r["title_en_text"];
		}
		if ( ($r["title_th"]=='อื่นๆ')||(!$r["title_th_text"]=='') ) {
			$r['title_th'] = $r["title_th_text"];
		}

		if ( !empty($_POST["member_company"]) ) {
			// $group_btn = '<a class="btn btn-info" title="Reset password" onClick="view_info('.$r["member_id"].','.$r["cid"].')"><i class="fa fa-list"></i></a>';
			$org_name = $r["org_name"];
			if ( preg_match("/^[1-9][0-9]*$/", $org_name) ) {
				$q = "SELECT name FROM receipt WHERE active='T' AND receipt_id={$org_name}";
				$org_name=$atiDB->data($q);
			}//end if

			if ( $r["fa"]=='T' && $r["private"]=='T' ) {
				$r['tel_home'] = "";
				$r['email1'] = "";
			}//end if

			if ( $_POST["member_company_id"] && $_POST["member_company_righttype"]==2 ) {
				global $db;
				$q = "SELECT company_name_th FROM member_company WHERE active='T' AND member_company_id={$_POST["member_company_id"]}";
				$company_name_th=$db->data($q);
				$company_name_th = trim($company_name_th);
				$$org_name = trim($org_name);
				
				if ( $org_name == $company_name_th ) {
					$a[] = array($runNo
					      ,$org_name
					      ,$full_name
					      ,$r['tel_home']
					      ,$r['email1']
						);
				}else{
					continue;
				}

			}elseif( $_POST["member_company_id"] && $_POST["member_company_righttype"]!=2 ){
				if ( $_POST["member_company_righttype"]==1 ) {			
					$a[] = array($runNo
						      ,$full_name
						      ,$org_name
						      ,$r['tel_home']
						      ,$r['email1']
							);
				}else{
					continue;
				}//end else

			}else{
				$a[] = array($runNo
					      ,$full_name
					      ,$org_name
					      ,$r['tel_home']
					      ,$r['email1']
						);
			}//end else


		}else{
			$a[] = array($runNo
				      ,$r["cid"]
				      ,$full_name
				      ,$r['title_en']." ".$r['fname_en']." ".$r['lname_en']
				      ,revert_date($r['reg_date'], true)
				      ,$manage." ".$button." ".$btn_reset_passwd." ".$viewimg
					);
		}

		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM member
			  $WHERE $sAND $sWhere";

$rs = $atiDB->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM member";
$resultTotal = $atiDB->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
