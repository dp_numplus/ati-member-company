<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/person.php";
global $db;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["person_id"];
	if($id){
	   $r = get_person("", $id);
	   foreach($r as $k=>$v){
	      $aData[] = $v;

	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 || $i==4)
		return "a.person_id";
	else if ( $i == 1 )
		return "a.code";
	else if ( $i == 2)
		return "a.name";
	else if ( $i == 3 )
		return "a.company_id";
	else return "a.person_id";
}


$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}

/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "(a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%') ";
	$sAND = "AND ";
}
$sWhere .= ($_POST["company_id"]) ? " and a.company_id={$_POST["company_id"]}" : "";
$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";


/* Paging */
$sQuery = "SELECT a.person_id, a.code, a.name, a.remark, a.active, a.company_id
				 ,c.name as company_name, a.phone, a.mobile
           FROM person a 
           		left join company c on c.company_id=a.company_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	$list_id = get_config('sectionDisplayButton');
	$array_ck = explode(",", $list_id);
	
	
	foreach ($rResult as $r){
	  $id = $r["person_id"];
	  $manage =   get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
	  $ck = $r["section_id"];
	  if(in_array($ck, $array_ck)) $manage = $manage; 
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      ,$r["company_name"]
				      ,$r['phone']."<br>".$r["mobile"]
				      ,$r['remark']
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM person a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM person a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
