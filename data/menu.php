<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/menu.php";
global $db;
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["menu_id"];
	if($id){
		$q = "select a.menu_id, a.code, a.name, a.name_eng, a.active, a.useclass, a.action, a.url
				,a.recby_id, a.rectime, a.active
			from menu a 
			where a.active!='' and a.menu_id=$id";
		$aData = $db->get($q);
	}
}else{
  $aColumns = array( 'menu_id','code','name','name_eng','active');
  /* Indexed column (used for fast and accurate table cardinality) */
  $sIndexColumn = "menu_id";

  function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0)
	  return "menu_id";
	else if ( $i == 1 )
	  return "code";
	else if ( $i == 2 )
	  return "name";
	else if ( $i == 3 )
	  return "name_eng";
	else if ( $i == 4 )
	  return "active";
  }

  $sLimit = "";
  if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
  {
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
  }

  /* Ordering */
  if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
	  $sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
				  ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
  }
   
  /* Filtering */
  $sWhere = "";
  if($_POST['sSearch'] != ""){
	 $sWhere = "code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			"name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			"name_eng LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			"active LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
	$WHERE = "WHERE ";
  }
  /* Paging */
  $sQuery = "SELECT menu_id, code, name, name_eng, active
		 FROM menu
		 $WHERE $sWhere
		 $sOrder
		 $sLimit";

  $rResult = $db->get($sQuery);
  $a = array();
  if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
	  $id = $r["menu_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
	  $a[] = array($runNo
				,$r['code']
				,$r['name']
				,$r['name_eng']
				,$active
				,$manage);
	  $runNo++;
	}
  }

  $aData = array();
  $sQuery = "SELECT COUNT(*) as total
		  FROM menu
		  $WHERE $sWhere";

  $rs = $db->data($sQuery);
  $iFilteredTotal = $rs;
   
  $sQuery = "SELECT COUNT(*) as total
		  FROM menu";
  $resultTotal = $db->data($sQuery);
  $iTotal = $resultTotal;
			   
  $aData["sEcho"] = intval($_POST['sEcho']);
  $aData["iTotalRecords"] = $iTotal; 
  $aData["iTotalDisplayRecords"] = $iFilteredTotal; 
  $aData["aaData"] = $a; 
}

echo json_encode($aData);
?>
