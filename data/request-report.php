<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/request.php";
global $db, $RIGHTTYPEID, $MEMBERID;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["request_id"];
	if($id){
		$r = get_request("", $id);
		foreach($r as $k=>$v){
			$v["date_start_time"] = substr($v["date_start"], 11, -3);
			$v["date_stop_time"] = substr($v["date_stop"], 11, -3);
			$v["date_return_time"] = substr($v["date_return"], 11, -3);
			if($v["date_start"]!='0000-00-00 00:00:00'){
				$v["date_start"] = revert_date($v["date_start"]);
			}else { 
				$v["date_start"] = "";
				$v["date_start_time"] = "";
			}
			if($v["date_stop"]!='0000-00-00 00:00:00'){	   		
				$v["date_stop"] = revert_date($v["date_stop"]);
			} else {	   		
				$v["date_stop"]  = "";
				$v["date_stop_time"] = "";
			}

			if($v["date_return"]!='0000-00-00 00:00:00' && $v["date_return"]!=""){
				$v["date_return"] = revert_date($v["date_return"]);
			}
			else{ 
				$v["date_return"]  = "";
				$v["date_return_time"] = "";
			} 
			$aData[] = $v;

		}  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.request_id";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.title";
	else if ( $i == 4 )
		return "a.section_id";
	else if ( $i == 5 )
		return "a.requesttype_id";
}


$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
if($_POST["type"]=="request_id"){
	$sOrder = "ORDER BY  a.request_id asc";
} 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.title LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "c.fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ";
			    "c.lname LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
	$sWhere .= ($_POST["membertype_id"]) ? " and c.membertype_id={$_POST["membertype_id"]}" : "";
	$sWhere .= ($_POST["department_id"]) ? " and c.department_id={$_POST["department_id"]}" : "";
	if($_POST["active"]==4){
		$sWhere .= ($_POST["active"]) ? " and a.requeststatus_id='{$_POST["active"]}'" : "";
	}else{
		if($_POST["type"]!="ret"){
			$sWhere .= ($_POST["active"]) ? " and a.requeststatus_id='{$_POST["active"]}'" : "";
		}else if($_POST["type"]!="request"){
			$sWhere .= ($_POST["active"]) ? " and a.requeststatus_id='{$_POST["active"]}'" : "";
		}else{
			$sWhere .= ($_POST["active"]) ? " and a.requeststatus_id>='{$_POST["active"]}'" : "";
		}		
	}	
	

$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  date("Y-01-01");
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : date("Y-12-31");
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and a.docdate>='$dateStart' and a.docdate<='$dateStop'" : "";
$sWhere .= ($RIGHTTYPEID==3) ? " and a.member_id='$MEMBERID'" : "";


/* Paging */
$sQuery = "select a.request_id,
				a.runyear,
				a.runno,
				a.docno,
				a.dept_phone,
				a.docdate,
				a.title,
				a.date_start,
				a.date_stop,
				a.date_return,
				a.member_id,
				a.room_id,
				a.room_other,
				a.servicecharge_id,
				a.servicecharge_detail,
				a.returnapprove_id,
				a.returnapprovetime,
				a.requestapprove_id,
				a.requestapprovetime,
				a.cancelby_id,
				a.canceltime,
				a.requeststatus_id,
				a.remark,
				a.recby_id,
				a.rectime,
				a.active,
				a.requeststate_id,
				a.detail,
				a.set_time,
				b.name as requeststatus_name,
				concat(c.prefix,' ',c.`fname`,' ',c.lname) as membername,
				c.code as membercode,
                d.name as department_name,
                e.name as membertype_name,
                f.name as roomname
		 from request a left join requeststatus b on b.requeststatus_id=a.requeststatus_id
		 	  left join member c on c.member_id=a.member_id
			  left join department d on d.department_id=c.department_id
			  left join membertype e on e.membertype_id=c.membertype_id
			  left join room f on f.room_id=a.room_id	
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	$list_id = get_config('sectionDisplayButton');
	$array_ck = explode(",", $list_id);
	
	
	foreach ($rResult as $r){
	  $id = $r["request_id"];
	  $manage =  get_datatable_icon("print", $id);
			if($r["date_start"]!='0000-00-00 00:00:00'){
				$r["date_start"] = revert_date($r["date_start"], true);
			}else { 
				$r["date_start"] = "";
			}
			if($r["date_stop"]!='0000-00-00 00:00:00'){	   		
				$r["date_stop"] = revert_date($r["date_stop"], true);
			} else {	   		
				$r["date_stop"]  = "";
			}

			if($r["date_return"]!='0000-00-00 00:00:00' && $r["date_return"]!=""){
				$r["date_return"] = revert_date($r["date_return"], true);
			}
			else{ 
				$r["date_return"]  = "";
			} 
		$a[] = array($runNo
				      ,$r['docno']
				      ,revert_date($r['docdate'])
				      ,$r['title']
				      ,$r['membername']
				      ,$r['department_name']
				      ,$r["roomname"]
				      ,$r["detail"]
				      ,$r["date_start"]." <br> ".$r["date_stop"]
				      ,$r["requeststatus_name"]
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = " SELECT COUNT(a.request_id) as total
			FROM request a left join requeststatus b on b.requeststatus_id=a.requeststatus_id
			left join member c on c.member_id=a.member_id
			left join department d on d.department_id=c.department_id
			left join membertype e on e.membertype_id=c.membertype_id
			left join room f on f.room_id=a.room_id	
			$WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM request a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
