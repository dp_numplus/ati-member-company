<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/emp.php";
global $db;
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["emp_id"];
	if($id){
	   $r = emp_info("", $id);
	   foreach($r as $k=>$v){
	      $v["birthdate"] = revert_date($v["birthdate"]);
		  if(!$v["img"])
		     $v["img"] = "images/no-avatar-male.jpg";
		  $con = " and a.emp_id=$id";
		  $t = login_info($con);
		  if($t){
			foreach($t as $key=>$val){
				$v["activelogin"] = $val["active"];
				$v["righttype_id"] = $val["righttype_id"];
				$v["username"] = $val["username"];
				$v["password"] = $val["password"];
				$v["section_id"] = $val["section_id"];			
			}
		  }else{
				$v["activelogin"] = "";
				$v["righttype_id"] = "";
				$v["username"] = "";
				$v["password"] = "";  	
		  }
		  
	      $aData[] = $v;
	   }  
	}
}else{
  $aColumns = array( 'emp_Id','prefix','fname','lname','cid');
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "empId";

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "emp_id";
	else if ( $i == 2 )
		return "prefix";
	else if ( $i == 3 )
		return "fname";
	else if ( $i == 4 )
		return "lname";
	else if ( $i == 5 )
		return "cid";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
$sWhere = "";
if($_POST['sSearch'] != ""){
   $sWhere = "org_name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "lname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "phone LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
	$WHERE = "WHERE ";
}
/* Paging */
$sQuery = "SELECT emp_id
			, prefix
			, fname
			, lname
			, cid
			, phone
			, email
			, org_name
			, position
           FROM emp
		   $WHERE $sWhere
		   $sOrder
		   $sLimit";
// echo $sQuery;die();
$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["emp_id"]; 
	  $manage = '<a class="btn btn-default" title="LINK ไปยังสำนักงาน ก.ล.ต." onClick="link_url('.$id.')"><i class="fa fa-link"></i> LINK</a>';
	  $manage .=  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
		$a[] = array($runNo
				      ,$r['org_name']
				      ,$r['position']
				      ,$r['prefix']." ".$r['fname']." ".$r['lname']
				      ,$r["phone"]
				      // ,$r["email"]
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM emp
			  $WHERE $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM emp";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
