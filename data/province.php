<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
include_once "../share/datatype.php";

$data = array();
$id = $_GET['value'];
$qtype = $_GET['name'];
if($qtype == 'amphur') {
    if($id!=""){
    	$con = " and a.active='T' and a.province_id={$id}";
 		$amphur = datatype($con, "amphur", true);
        if($amphur>0){
        	foreach ($amphur as $key => $v) {
        		 $data[] = array($v["amphur_id"],$v["name"]);
        	}
        }
    }   
}
if($qtype == 'district') {
    if($id!=""){
    	$con = " and a.active='T' and a.amphur_id={$id}";
 		$district = datatype($con, "district", true);
        if($district>0){
        	foreach ($district as $key => $v) {
        		 $data[] = array($v["district_id"],$v["name"]);
        	}
        }
    }   
}
 
echo json_encode($data);
?>