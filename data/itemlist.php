<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/item.php";
global $db;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["item_id"];
	if($id){
	   $r = get_item("", $id);
	   foreach($r as $k=>$v){
	      $aData[] = $v;

	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 || $i==4)
		return "a.item_id";
	else if ( $i == 1 )
		return "a.code";
	else if ( $i == 2)
		return "a.name";
	else if ( $i == 3 )
		return "a.itemtype_id";
	else if ( $i == 5 )
		return "a.room_id";
}


$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
if($_POST["type"]=="item_order"){
	$sOrder = "ORDER BY  a.item_order asc";
} 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "(a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
  			 "a.address LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
  			 "a.itemtype_text LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%') ";
	$sAND = "AND ";
}
$sWhere .= ($_POST["itemtype_id"]) ? " and a.itemtype_id={$_POST["itemtype_id"]}" : "";
$sWhere .= ($_POST["room_id"]) ? " and a.room_id={$_POST["room_id"]}" : "";
$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";


/* Paging */
$sQuery = "SELECT a.item_id, a.code, a.name, b.name as room_name, a.remark, a.active, a.itemtype_id, a.itemtype_text
				 ,c.name as itemtype_name, a.custom_order
           FROM item a left join room b on b.room_id=a.room_id
           		left join itemtype c on c.itemtype_id=a.itemtype_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	$list_id = get_config('sectionDisplayButton');
	$array_ck = explode(",", $list_id);
	
	
	foreach ($rResult as $r){
	  $id = $r["item_id"];
	  $manage =   get_datatable_icon("edit", $id);
	  if($_POST["type"]=="item_order"){
	  	$manage = '<input class="form-control" type="text" name="menu_order_'.$id.'" data="'.$r["custom_order"].'" value="'.$r["custom_order"].'" style="width:50px; display:inline-block;"><a class="btn btn-info" onClick="update_order(\''.$id.'\')"><i class="fa fa-sort"></i> </a>';
	  }
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
	  $ck = $r["section_id"];
	  if(in_array($ck, $array_ck)) $manage = $manage; 
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      ,($r["itemtype_id"]) ? $r["itemtype_name"] : $r["itemtype_text"]
				      ,$r['remark']
				      ,$r['room_name']
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM item a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM item a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
