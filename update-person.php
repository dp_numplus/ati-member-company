<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;

if ( $_POST["type"]=="del" ) {
	$args["table"] = "person";
	$args["id"] = (int)$_POST["person_id"];
	$args["company_id"] = (int)$_POST["company_id"];
	$args["active"] = "F";
	$ret = $db->set($args);
	die();
}//end if

$code = trim($_POST["code"]);
$dup = 0;
$id = "";
/*if($code && $_POST["active"]!='F'){
	$id = $_POST["person_id"];
	$con = ($id) ? " and person_id != $id" : "";
	$q = "select count(person_id) as c from person where code='$code' $con and active!='F'";
	$rs = $db->data($q);
	if($rs>0) $dup = 1;
}
if(!$code || $dup==1){
	$args = array();
	$args["p"] = "person";
	$args["person_id"] = $id;
	$args["type"] = "info";
	$_SESSION["error"]["msg"] = "รหัส $code ซ้ำ";
	redirect_url($args);
}
*/
if($_POST){
	$args = array();
	$args["table"] = "person";
	if($_POST["person_id"]){
	   $args["id"] = $_POST["person_id"];
	}
	$args["code"] = $_POST["code"];
	$args["company_id"] = (int)$_POST["company_id"];
	$args["name"] = $_POST["name"];
	$args["position"] = $_POST["position"];
	$args["phone"] = $_POST["phone"];
	$args["mobile"] = $_POST["mobile"];
	$args["ext"] = $_POST["ext"];
	$args["email"] = $_POST["email"];
	$args["remark"] = $_POST["remark"];
	$args["main"] = ($_POST["main"]=="T") ? $_POST["main"] : "F";
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	
   $ret = $db->set($args);
   $person_id = $args["id"] ? $args["id"] : $ret;
}
$_SESSION["success"]["msg"] = "Updated Successfully";

$args = array();
$args["p"] = "company";
$args["person_id"] = $person_id;
$args["company_id"] = $_POST["company_id"];
$args["type"] = "personinfo";
redirect_url($args);
?>