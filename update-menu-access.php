<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";
global $db;

$righttype_id = $_POST["righttype_id"];
$tmp_name = $_POST["righttypename"];

$db->begin();
if($righttype_id){
	if(is_array($_POST["ckMenu"])){
		foreach($_POST["ckMenu"] as $key=>$val){
			$args = array();
			$args["table"] = "menuvisible";
			if($_POST["menuvisible_id"][$key])
				$args["id"] = $_POST["menuvisible_id"][$key];
			$args["menu_id"] = (int)$_POST["menu_id"][$key];
			$args["righttype_id"] = (int)$righttype_id;
			$args["recby_id"] = (int)$EMPID;
			$args["rectime"] = date("Y-m-d H:i:s");
			if(!$args["id"] && $val=="F") continue;
			if($args["id"] && $val=="F"){
				$id = $args["id"];
				$q = "select menuvisible_id from menuvisible where menuvisible_id=$id";
				$testId = $db->data($q);
				if($testId){
				   $db->query("delete from menuvisible where menuvisible_id=$testId");
				   continue;
				}				
			}
			$db->set($args);
		}
		
	}

	if(is_array($_POST["ckMenuList"])){
		foreach($_POST["ckMenuList"] as $index=>$v){
			$args = array();
			$args["table"] = "menuvisible";
			if($_POST["menuvisiblelist_id"][$index])
				$args["id"] = $_POST["menuvisiblelist_id"][$index];
			$args["menulist_id"] = (int)$_POST["menulist_id"][$index];
			$args["righttype_id"] = (int)$righttype_id;
			$args["recby_id"] = (int)$EMPID;
			$args["rectime"] = date("Y-m-d H:i:s");
			if(!$args["id"] && $v=="F") continue;
			if($args["id"] && $v=="F"){
				$id = $args["id"];
				$q = "select menuvisible_id from menuvisible where menuvisible_id=$id";
				$testId = $db->data($q);
				if($testId){
					$db->query("delete from menuvisible where menuvisible_id=$testId");
					continue;
				}
			}
			$db->set($args);
		}	
	}	
}
$db->commit();
$aArg = array();
$aArg["righttype_id"] = $righttype_id;
$aArg["righttypename"] = $tmp_name;	
redirect_url($aArg);
?>