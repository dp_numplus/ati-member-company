<?php 
function view_todo($con="", $todo_id="", $order=false){
   if($con=="" && $todo_id=="") return array();
   global $db;   
   $con_todo_id = $todo_id ? " and a.todo_id=$todo_id" : "";
   $con = $todo_id ? "" : $con;
   $con_orders = ($order==true) ? " a.todo_id " : " a.todo_id desc";
    $q = "select 
            a.todo_id,
            a.code,
            a.name,
            a.name_eng,
            a.detail,
            a.active,
            a.recby_id,
            a.rectime,
            a.remark,
            a.highlight,
            a.company_id,
            a.assign_to_id,
            a.target_date,
            a.created_date,
            a.todostatus_id,
            b.name as todostatus_name
    from  todo a left join todostatus b on a.todostatus_id=b.todostatus_id
    where a.active!='' $con $con_todo_id
    order by  $con_orders, highlight desc
    limit 400";
   $r = $db->get($q);   
   return $r;
}
?>
