<?php 
function view_news($con="", $news_id="", $order=false){
   if($con=="" && $news_id=="") return array();
   global $db;   
   $con_news_id = $news_id ? " and a.news_id=$news_id" : "";
   $con = $news_id ? "" : $con;
   $con_orders = ($order==true) ? " a.news_id " : " a.news_id desc";
    $q = "select 
            a.news_id,
            a.code,
            a.name,
            a.name_eng,
            a.detail,
            a.section_id,
            a.link_to,
            a.newstype_id,
            a.active,
            a.recby_id,
            a.rectime,
            a.remark,
            a.highlight,
            a.company_id,
            a.quotation_no,
            a.assign_to_id,
            a.target_date,
            a.created_date,
            a.newsstatus_id,
            b.name as newsstatus_name,
            CONCAT(c.prefix, c.fname, ' ', c.lname) as assign_to_name
    from  news a left join newsstatus b on a.newsstatus_id=b.newsstatus_id
    left join emp c on a.assign_to_id=c.emp_id
    where a.active!='' $con $con_news_id
    order by  $con_orders, highlight desc
    limit 400";

   $r = $db->get($q);   
   return $r;
}
?>
