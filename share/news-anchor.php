<?php 
function view_news_anchor($con="", $news_anchor_id="", $order=false){
   if($con=="" && $news_anchor_id=="") return array();
   global $db;   
   $con_news_anchor_id = $news_anchor_id ? " and a.news_anchor_id=$news_anchor_id" : "";
   $con = $news_anchor_id ? "" : $con;
   $con_orders = ($order==true) ? " a.news_anchor_id " : " a.news_anchor_id desc";
    $q = "select 
            a.news_anchor_id,
            a.code,
            a.name,
            a.name_eng,
            a.detail,
            a.section_id,
            a.link_to,
            a.news_anchortype_id,
            a.active,
            a.recby_id,
            a.rectime,
            a.remark,
            a.highlight
    from  news_anchor a 
    where a.active!='' $con $con_news_anchor_id
    order by a.highlight desc, $con_orders
    limit 400";

   $r = $db->get($q);   
   return $r;
}
?>
