<?php 
function view_member($con="", $member_id="", $order=false){
   if($con=="" && $member_id=="") return array();
   global $db;   
   $conmember_id = $member_id ? " and a.member_id=$member_id" : "";
   $con = $member_id ? "" : $con;
   $conorder = ($order==true) ? " a.member_id " : " a.member_id desc";
   $q = "select a.member_id, a.code, a.prefix, a.fname, a.lname, a.birthdate, a.active, a.address
                 ,a.cid, a.email, a.homephone, a.nickname, a.phone, a.recby_id, a.rectime
                 ,a.active_login, a.cart_no_file_part, a.cart_no_file_part as cart_no_file_part_tmp, a.username, a.password
                 ,a.university_id, c.name as university_name
                 ,a.educationlevel_id, d.name as educationlevel_name
                 ,a.receive_file_part, a.receive_file_part as receive_file_part_tmp
                 ,a.line, a.bank_id, a.bank_no, e.name as bank_name
                 ,a.guide
           from member a left join member b on a.recby_id=b.member_id
                left join university c on a.university_id=c.university_id
                left join educationlevel d on d.educationlevel_id=a.educationlevel_id
                left join bank e on e.bank_id=a.bank_id
         where a.active!='' $con $conmember_id
         order by $conorder
         limit 100";
   $r = $db->get($q);   
   return $r;
}
function get_work_time($con="", $work_time_id="", $order=false){
   if($con=="" && $work_time_id=="") return array();
   global $db;   
   $conwork_time_id = $work_time_id ? " and a.work_time_id=$work_time_id" : "";
   $con = $work_time_id ? "" : $con;
   $conorder = ($order==true) ? " a.work_time_id desc" : " a.work_time_id";
   $q = "select a.work_time_id, a.work_time_type, a.date_job_start, a.date_job_stop, a.time_job, a.remark, a.member_id
           from work_time a
         where a.active!='' $con $conwork_time_id
         order by $conorder
         limit 100";
         /*echo $q;*/
   $r = $db->get($q);   
   return $r;
}

?>