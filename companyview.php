<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/person.php";
include_once "./share/news.php";
global $db;
$section = datatype(" and a.active='T'", "section", true);
$company_id = $_GET["company_id"];
$q = "select  name, map_img from company where company_id=$company_id";
$r = $db->rows($q);
$str = "";
if($r){
	$link_img = $r["map_img"];
	$str = $r["name"];
}else{
	$str = "Add New";
}
?>
<div id="cl-wrapper">
	<div class="container" id="pcont">


		<div class="cl-mcont" style="width:1280px; margin:0 auto;">
			<div class="col-md-12">  		
				<img src="images/logo-fa.png" alt="">
			</div>
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">					  
					 
					  <div class="content" style="padding-top: 0px">
					  	<ol class="breadcrumb" style="margin-bottom: 0px">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">Home</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>							
						  <form id="frmMain"  name="frmMain" class="form-horizontal"  method="post" enctype="multipart/form-data" action="update-company.php">
						  <input type="hidden" name="company_id" id="company_id" value="<?php echo $company_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						  <div class="col-sm-12">
						  	<div class="header">							
						  		<h3>บริษัทสมาชิกชมรมวาณิชธนกิจ</h3>
						  	</div>
							  <div class="form-group row">		                
								<label class="col-sm-2 control-label">ชื่อบริษัท(ไทย)</label>
								<div class="col-sm-8">
								  <input class="form-control" name="name" id="name" required="" placeholder="ชื่อบริษัท(ไทย)" type="text">
								</div>	                			                
							  </div>
							  <div class="form-group row">
								<label class="col-sm-2 control-label">ที่อยู่</label>
								<div class="col-sm-8">
								  <textarea class="form-control" id="address" name="address"></textarea>
								</div>		                
							  </div>
							  	<div class="form-group row">							  	
							  		<label class="col-sm-2 control-label">E-Mail</label>
							  		<div class="col-sm-3">
							  			<input class="form-control" name="email" id="email" placeholder="E-mail" type="email">
							  		</div>
							  		<label class="col-sm-2 control-label">หมายเลขโทรศัพท์</label>
							  		<div class="col-sm-3">
							  			<input class="form-control" name="phone" id="phone" placeholder="หมายเลขโทรศัพท์" type="text">
							  		</div>
							  	</div>
							  	<div class="form-group row">
							  		<label class="col-sm-2 control-label">เว็บไซต์</label>
							  		<div class="col-sm-3">
							  			<input class="form-control" name="website" id="website" placeholder="www.test.com">
							  		</div>
							  	</div>
							  	<hr>
							  	<div class="form-group row">
							  		<label class="col-sm-2 control-label">ทุนจดทะเบียน (ล้านบาท)</label>
							  		<div class="col-sm-3">
							  			<input class="form-control" name="capital" id="capital" placeholder="ทุนจดทะเบียน (ล้านบาท)" type="text">
							  		</div>
							  		<label class="col-sm-2 control-label">ส่วนของผู้ถือหุ้น (%)</label>
							  		<div class="col-sm-3">
							  			<input class="form-control" name="ratio" id="ratio" placeholder="ส่วนของผู้ถือหุ้น (%)" type="text">
							  		</div>
							  	</div>
							  	<hr>
							  	<div class="form-group row">
							  		<label class="col-sm-2 control-label">ข้อมูลเพิ่มเติม</label>
							  		<div class="col-sm-8">
							  		<textarea rows="5" class="form-control" id="tax_address" name="tax_address"></textarea>							  			
							  		</div>
							  		
							  	</div>
							  </div>
							<hr>
							  <div class="form-group">
							
							  </div>
						  </div>
	
						</form>
					  </div>
					</div>
				<?php if($company_id){ ?>					
				  </div>

				  	<div class="block-flat">
				  		<div class="header">							
				  			
				  			<h3>รายชื่อบุคลากร</h3>
				  		</div>	
				  		<div class="content">
				  			<table class="table table-striped" id="tbList">
				  				<thead>
				  					<tr class="alert alert-success" style="font-weight:bold;">
				  						<td width="5%" class="center">ลำดับ</td>
				  						<td width="20%" class="center">ชื่อ-นามสกุล</td>
				  						<td width="20%" class="center">ตำแหน่ง</td>
				  						
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<?php $r =  $r = get_person("and a.active='T' and a.company_id=$company_id");
				  					if($r){
				  						$runNo = 1; 
				  						foreach($r as $k=>$v){
				  							?>
				  							<tr style="cursor:pointer;">
				  								<td class="center"><?php echo $runNo; ?></td>
				  								<td class="center"><?php echo $v["name"] ?></td>
				  								<td  class="center"><?php echo $v["position"]; ?></td>
				  								
				  							</tr>
				  							<?php
				  							$runNo++;
				  						} 	
				  					}
				  					?>
				  				</tbody>
				  			</table>

				  		</div>
				  	</div>	
			



			<?php } ?>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>

<style> #head-nav{display: none;}</style>
<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var company_id = "<?php echo $_GET["company_id"]?>";
   if(company_id) viewInfo(company_id);

 });
function readURL(input) {
 if (input.files && input.files[0]) {
  var reader = new FileReader();
  reader.onload = function(e) {
   $('#map_img').attr('src', e.target.result);
 }
 reader.readAsDataURL(input.files[0]);
}
}
 function viewInfo(company_id){
   if(typeof company_id=="undefined") return;
   getcompanyInfo(company_id);
}

function getcompanyInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/companylist.php";
	var param = "company_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}

function removeImg(){
  var defaultImg = "images/no-avatar-male.jpg";
  if($('input[name=tmpimg]').val()=="")return;
  var t = confirm("ลบรูปภาพ");
  if(!t) return;
  $('#map_img').attr('src', defaultImg);
  $('input[name=delimg]').val("T");	
  ckSave();
}
function ckSave(id){
  onCkForm("#frmMain");
  $("#frmMain").submit();
}
function add_person(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=personinfo&company_id=<?php echo $company_id; ?>";
   redirect(url);
}
function add_news(newstype){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=newsinfo&company_id=<?php echo $company_id; ?>&newstype="+newstype;
   redirect(url);
}
function edit_person(id){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=personinfo&company_id=<?php echo $company_id; ?>&person_id="+id;
   redirect(url);
}
function edit_news(id, newstype){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=newsinfo&company_id=<?php echo $company_id; ?>&news_id="+id+"&newstype="+newstype;
   redirect(url);
}
</script>