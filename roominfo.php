<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/room.php";
global $db;
global $SECTIONID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$room_id = $_GET["room_id"];
$roomtype = datatype(" and a.active='T'", "roomtype", true);
$q = "select roomtype_id from room where room_id=$room_id";
$select = $db->data($q);
$q = "select  name from room where room_id=$room_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["name"];
}else{
	$str = "เพิ่มข้อมูลสถานที่";
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-room.php">
							<input type="hidden" name="room_id" id="room_id" value="<?php echo $room_id; ?>">
							<div class="col-sm-12">
								<div class="form-group row">
								<label class="col-sm-1 control-label" style="width:115px;">รหัสสถานที่ <span class="red">*</span></label>
								<div class="col-sm-2" style="width:150px;">
									<input class="form-control" id="code" name="code" required="" placeholder="รหัสสถานที่" type="text">
								</div>
              
								<label class="col-sm-2 control-label">อาคาร<span class="red">*</span></label>
								<div class="col-sm-4">
									<select name="roomtype_id" id="roomtype_id" class="select2 required">
										<option value="">---- เลือก ----</option>
										<?php foreach ($roomtype as $key => $value) {
											$id = $value['roomtype_id'];
											$s = "";
											if($select==$id){
												$s = "selected";
											}
											$name = $value['name'];
											echo  "<option value='$id' {$s}>$name</option>";
										} ?>

									</select>
								</div>
								</div>                        
								<div class="form-group row"> 
                                      
									<label class="col-sm-1 control-label" style="width:115px;">ชื่อสถานที่ <span class="red" style="display:inline-block; position:absolute;"> &nbsp;*</span></label>
									<div class="col-sm-5">
										<input class="form-control" name="name" id="name" required="" placeholder="ชื่อสถานที่" type="text">
									</div>                                            
									<label class="col-sm-1 control-label">ความจุ</label>
									<div class="col-sm-4">
										<input class="form-control" name="amount" id="amount"  placeholder="ความจุ" type="text">
									</div>
								</div> 
								<div class="form-group row">
								  <label class="col-sm-1 control-label" style="width:115px;">หมายเหตุ</label>
								
								  	<div class="col-sm-10">
								  		<textarea id="remark" class="form-control" name="remark" placeholder="หมายเหตุ"></textarea>
								  	</div>
							  </div>                       
								<div class="form-group row"> 
									<label class="col-sm-1 control-label">สถานะ <span class="red">*</span></label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
											<option selected="selected" value="T">active</option>
											<option value="F">inActive</option>
										</select>
									</div>
								</div>								
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	 $("#frmMain").validate();
	 var room_id = "<?php echo $_GET["room_id"]?>";
	 if(room_id) viewInfo(room_id);
	 //$('#remark').summernote({height: 80});
     var error = "<?php echo $error ?>";
     if(error!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: error,
		        class_name: 'danger'
		      });
	        }
	     });
     }
     var success = "<?php echo $success ?>";
     if(success!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: success,
		        class_name: 'success'
		      });
	        }
	     });
     }


 });
var trMenuList = $("#tbMenuList tbody tr:eq(0)").clone();
delRow("#tbMenuList");
 function viewInfo(room_id){
	 if(typeof room_id=="undefined") return;
	 getInfo(room_id);
}

function getInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/roomlist.php";
	var param = "room_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");

}

function ckSave(id){
	onCkForm("#frmMain");
	//var tmp = $('#remark').code();
	//$('#remark').val(tmp);
	$("#frmMain").submit();
} 
</script>