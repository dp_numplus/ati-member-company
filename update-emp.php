<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;
if (!empty($_FILES)) {
	// เริ่มต้นใช้งาน class.upload.php ด้วยการสร้าง instant จากคลาส
	$upload_image = new upload($_FILES['empimg']) ; // $_FILES['image_name'] ชื่อของช่องที่ให้เลือกไฟล์เพื่ออัปโหลด 
	//  ถ้าหากมีภาพถูกอัปโหลดมาจริง
	if ( $upload_image->uploaded ) { 
		// ย่อขนาดภาพให้เล็กลงหน่อย  โดยยึดขนาดภาพตามความกว้าง  ความสูงให้คำณวนอัตโนมัติ
		// ถ้าหากไม่ต้องการย่อขนาดภาพ ก็ลบ 3 บรรทัดด้านล่างทิ้งไปได้เลย
		$upload_image->file_src_name_body = "EMP-".$_POST["code"]."-".time();
		$upload_image->image_resize         = true ; // อนุญาติให้ย่อภาพได้
		$upload_image->image_x              = 150 ; // กำหนดความกว้างภาพเท่ากับ 400 pixel 
		$upload_image->image_ratio_y        = true; // ให้คำณวนความสูงอัตโนมัติ

		$upload_image->process( "empImg/" ); // เก็บภาพไว้ในโฟลเดอร์ที่ต้องการ  *** โฟลเดอร์ต้องมี permission 0777

		// ถ้าหากว่าการจัดเก็บรูปภาพไม่มีปัญหา  เก็บชื่อภาพไว้ในตัวแปร เพื่อเอาไปเก็บในฐานข้อมูลต่อไป
		if ( $upload_image->processed ) {

			$image_name =  $upload_image->file_dst_name; // ชื่อไฟล์หลังกระบวนการเก็บ จะอยู่ที่ file_dst_name
			$upload_image->clean(); // คืนค่าหน่วยความจำ

		}// END if ( $upload_image->processed )

	}
}
/*print_r($_FILES);
echo "<hr>";
print_r($_POST);
die();*/
if($_POST){
    if($_POST["delimg"]=="T"){
	   if(file_exists($_POST["tmpimg"]))
	      unlink($_POST["tmpimg"]);
	   $_POST["tmpimg"] = "";
    }
	$args = array();
	$args["table"] = "emp";
	if($_POST["emp_id"])
	   $args["id"] = $_POST["emp_id"];
	/*$args["code"] = $_POST["code"];*/
	$args["prefix"] = $_POST["prefix"];
	$args["fname"] = $_POST["fname"];
	$args["lname"] = $_POST["lname"];
	//$args["nickname"] = $_POST["nickname"];
/*	$args["cid"] = $_POST["cid"];
	$args["birthdate"] = ($_POST["birthdate"] ? thai_to_timestamp($_POST["birthdate"]) : "");*/
	$args["email"] = $_POST["email"];
	$args["address"] = $_POST["address"];
	$args["active"] = $_POST["active"];
	$args["phone"] = $_POST["phone"];
	$args["position"] = trim($_POST["position"]);
	$args["org_name"] = trim($_POST["org_name"]);
	//$args["homephone"] = $_POST["homephone"];
	$args["recby_id"] = (int) $emp_id;
	$args["rectime"] = date("Y-m-d H:i:s");
	//$args["img"] = $image_name ? "empImg/$image_name" : $_POST["tmpimg"];
   $ret = $db->set($args);
   $emp_id = $args["id"] ? $args["id"] : $ret;
	if($emp_id){
		$args = array();
		$args["table"] = "login";
		$q = "select login_id from login where emp_id=$emp_id order by login_id desc";
		$login_id = $db->data($q);
		if($login_id)
			$args["id"] = $login_id;
		else
			$args["emp_id"] = $emp_id;
		$args["username"] = $_POST["username"];
		$args["password"] = $_POST["password"];
		$args["righttype_id"] = (int)$_POST["righttype_id"];
		$args["active"] = $_POST["activelogin"];
			$args["recby_id"] = (int)$emp_id;
	    $args["rectime"] = date("Y-m-d H:i:s");
		$db->set($args);
	}
}
$args = array();
$args["p"] = "emp";
$args["emp_id"] = $emp_id;
$args["type"] = "info";
redirect_url($args);
?>